export const state = () => ({
  badges: []
})

export const mutations = {
  setBadges(state, badges) {
    state.badges = badges
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { app }) {
    const badges = await app.$axios.$get(
      "./badges.json"
    )
    commit("setBadges", badges)
  }
}
