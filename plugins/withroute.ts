import {Route} from 'vue-router'

export default interface WithRoute /* can also extend Vue to make sure nothing is colliding */ {
	$route: Route
}